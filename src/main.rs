use actix_files as fs;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;
use serde::Deserialize;

#[derive(Deserialize)]
struct Guess {
    color: String,
}

async fn index() -> impl Responder {
    fs::NamedFile::open("index.html").unwrap()
}

async fn guess_color(query: web::Query<Guess>) -> impl Responder {
    let colors = ["red", "blue", "green", "yellow"];
    let mut rng = rand::thread_rng();
    let selected_color = colors[rng.gen_range(0..colors.len())];

    if query.color == selected_color {
        HttpResponse::Ok().content_type("text/plain").body("Congratulations! You guessed correctly.")
    } else {
        HttpResponse::Ok().content_type("text/plain").body(format!("Sorry, the color was {}. Try again!", selected_color))
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/guess", web::get().to(guess_color))
            .service(fs::Files::new("/static", ".").show_files_listing())
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
