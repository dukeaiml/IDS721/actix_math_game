[![pipeline status](https://gitlab.com/dukeaiml/IDS721/zhankai_ye_mini_project_3/-/wikis/uploads/8b9be60c3dabf89331bc0c118cae1e44/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/zhankai_ye_mini_project_4/-/commits/main)
# Rust Actix Web App for Color Guessing
In this project, I have made a "Guess the Color" game, where the server randomly selects a color from a list, and the user guesses the color by clicking one of several buttons on a web page. Each button represents a possible color choice. When the user clicks a button, they're taken to a new page that tells them whether they guessed correctly or not.
## Goals
* Containerize simple Rust Actix web app
* Build Docker image
* Run container locally

## Steps
### Step 1: Initialize Rust Project
* `cargo new <YOUR-PROJECT-NAME>`
* Edit the `Cargo.toml` to include Actix Web and other dependencies.
* Implement Your Web App: Replace the content of `src/main.rs`. 
* Test Your Application using `cargo run`.

### Step 2: Create a Dockerfile
* Create a Dockerfile in the root of your project. This file will contain instructions for building the Docker image of your Actix web app.

### Step 3: Build the Docker Image
* Build the Docker Image. Open a terminal in the directory containing your Dockerfile and run: `docker build -t <YOUR-IMAGE-NAME> .`.
* Run the Container. After the image has been built, you can run it as a container. The following command runs the container and maps port 8080 inside the container to port 8080 on your host machine, allowing you to access the web application via `http://localhost:8080`. `docker run -d -p 8080:8080 <YOUR-IMAGE-NAME>`.

## Web App (Hosted via a container)
#### Web Function
![webapp](https://gitlab.com/dukeaiml/IDS721/actix_math_game/-/wikis/uploads/dd98c44105882105223d5e88752004b9/2.png)
![webapp2](https://gitlab.com/dukeaiml/IDS721/actix_math_game/-/wikis/uploads/f3395a6ac615ce565f37a81ff4c32434/3.png)
![webapp3](https://gitlab.com/dukeaiml/IDS721/actix_math_game/-/wikis/uploads/b1e63614ae8875fc53d12caf7f56d36c/4.png)
#### Docker Container and Image
![webapp4](https://gitlab.com/dukeaiml/IDS721/actix_math_game/-/wikis/uploads/0ba20d7c5ee1bb42f067fa604b914b4e/5.png)
![webapp5](https://gitlab.com/dukeaiml/IDS721/actix_math_game/-/wikis/uploads/b747cc2433760beb67a0180582dece76/7.png)
