# Use the official Rust image as the build environment
FROM rust:1-slim-bookworm AS builder

# Create a new empty shell project
RUN USER=root cargo new --bin actix_math_game
WORKDIR /actix_math_game

# Copy over your manifests
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock

# This build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# Now that the dependencies are built, copy your source code
COPY ./src ./src

# Build your application
RUN rm ./target/release/deps/actix_math_game*
RUN cargo build --release

# Use the Debian Buster image as the runtime environment
FROM bitnami/minideb:bookworm

# Copy the build artifact from the build stage and set the working directory
COPY --from=builder /actix_math_game/target/release/actix_math_game .

# Set the environment to production
ENV ROCKET_ENV=production

# Expose port 8080
EXPOSE 8080

# Run the binary
CMD ["./actix_math_game"]
